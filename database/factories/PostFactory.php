<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $ru_faker = \Faker\Factory::create('ru_RU');
        $fr_faker = \Faker\Factory::create('fr_FR');
        $de_faker = \Faker\Factory::create('de_DE');
        $cz_faker = \Faker\Factory::create('cz_CZ');
        return [
            'ru' => [
                'body' => 'RU --- ' . $ru_faker->paragraph(1),
            ],
            'en' => [
                'body' => 'EN --- ' . $this->faker->paragraph(1),
            ],
            'fr' => [
                'body' => 'FR --- ' . $fr_faker->paragraph(1),
            ],
            'de' => [
                'body' => 'DE --- ' . $de_faker->paragraph(1),
            ],
            'cz' => [
                'body' => 'CZ --- ' . $cz_faker->paragraph(1),
            ],
        ];
    }
}
