<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::factory()
            ->count(5)
            ->has(Post::factory()->count(5))
            ->create();

        $user = User::first();
        $ru_phrases = [
            'я люблю мороженое',
            'Сколько весит арбуз?',
            'Моя кошка самая красивая',
            'Я сижу за столом',
            'Пхп самый крутой язык',
            'Антон прочитал это сообщение',
            'Кто сильнее акула или медведь?',
            'Заборы такие большие',
            'На крыше сидят птицы',
            'Сначала была рифма, потом было слово',
        ];
        for ($i = 0; $i < count($ru_phrases); $i++){
            $data = ['ru' => ['body' => $ru_phrases[$i]], 'user_id' => $user->id];
            Post::create($data);
        }
    }
}
