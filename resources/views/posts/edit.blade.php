@extends('layouts.app')

@section('content')

    <form action="{{route('posts.update', ['post' => $post])}}" method="post">
        @method('put')
        @csrf

        <div class="form-group">
            <label for="ru[body]">@lang('messages.enter_phrase')</label>
            <input class="form-control" disabled type="text" name="ru[body]" id="ru[body]" value="{{$post->translate('ru')->body}}">
        </div>

        <div class="form-group">
            <label for="en[body]">@lang('messages.enter_translate_en')</label>
            <input required class="form-control" type="text" name="en[body]" id="en[body]">
        </div>

        <div class="form-group">
            <label for="fr[body]">@lang('messages.enter_translate_fr')</label>
            <input required class="form-control" type="text" name="fr[body]" id="fr[body]">
        </div>

        <div class="form-group">
            <label for="de[body]">@lang('messages.enter_translate_de')</label>
            <input required class="form-control" type="text" name="de[body]" id="de[body]">
        </div>

        <div class="form-group">
            <label for="cz[body]">@lang('messages.enter_translate_cz')</label>
            <input required class="form-control" type="text" name="cz[body]" id="cz[body]">
        </div>
        <br>
        <button class="btn btn-outline-primary" type="submit">@lang('messages.save')</button>

    </form>

@endsection
