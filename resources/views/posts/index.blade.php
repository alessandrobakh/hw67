@extends('layouts.app')

@section('content')

    @if(Auth::check())
    <h1>
        @lang('messages.hello') {{Auth::user()->name}}
    </h1>



    <a href="{{route('posts.create')}}">@lang('messages.add_new_post')</a>

    @foreach($posts as $post)
        @if($post->translations()->get()->count() < 5)
            <ul class="border-dark border my-3">
                <li>
                    {{$post->translate('ru')->body}} | <a href="{{route('posts.edit', ['post' => $post])}}">@lang('messages.translate')</a>
                </li>
            </ul>
        @endif
    @endforeach

    @endif

    @if($posts->count()>0)


        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>RU</th>
                <th>EN</th>
                <th>FR</th>
                <th>DE</th>
                <th>CZ</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                @if($post->translations()->get()->count() == 5)
            <tr>
                <td>{{$post->id}}</td>
                <td>{{$post->translate('ru')->body}}</td>
                <td>{{$post->translate('en')->body}}</td>
                <td>{{$post->translate('fr')->body}}</td>
                <td>{{$post->translate('de')->body}}</td>
                <td>{{$post->translate('cz')->body}}</td>
            </tr>
                @endif
            @endforeach
            </tbody>
        </table>

    @else

        <p>No posts</p>

    @endif

@endsection
