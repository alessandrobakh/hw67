@extends('layouts.app')

@section('content')

    <form action="{{route('posts.store')}}" method="post">
        @csrf

        <div class="form-group">
            <label for="ru[body]">@lang('messages.enter_phrase')</label>
            <input required class="form-control" type="text" name="ru[body]" id="ru[body]">
        </div>
        <br>
        <button class="btn btn-outline-primary" type="submit">@lang('messages.save')</button>

    </form>

@endsection
