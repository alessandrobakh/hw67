<?php

return [
    'hello' => 'Здравствуйте',
    'add_new_post' => 'Добавить новую фразу',
    'enter_phrase' => 'Введите фразу на русском',
    'login' => 'Войти',
    'register' => 'Регистрация',
    'logout' => 'Выйти',
    'laravel' => 'Ларавель',
    'translate' => 'Перевести',
    'enter_translate_en' => 'Введите перевод на английский',
    'enter_translate_fr' => 'Введите перевод на французский',
    'enter_translate_de' => 'Введите перевод на немецкий',
    'enter_translate_cz' => 'Введите перевод на чешский',
    'save' => 'Сохранить',
    'created' => 'Фраза добавлена!',
    'updated' => 'Фраза переведена!',
];
