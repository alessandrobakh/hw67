<?php

return [
    'hello' => 'Hello',
    'add_new_post' => 'Add new phrase',
    'enter_phrase' => 'Enter phrase in russian',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'laravel' => 'Laravel',
    'translate' => 'Translate',
    'enter_translate_en' => 'Enter translate in english',
    'enter_translate_fr' => 'Enter translate in french',
    'enter_translate_de' => 'Enter translate in german',
    'enter_translate_cz' => 'Enter translate in czech',
    'save' => 'Save',
    'created' => 'Phrase added!',
    'updated' => 'Phrase translated!',
];
